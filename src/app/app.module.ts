import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { CalendarComponent } from './module/calendar/calendar.component';
import { SmallCalendarComponent } from './module/calendar/small-calendar/small-calendar.component';
import { LargeCalendarComponent } from './module/calendar/large-calendar/large-calendar.component';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    SmallCalendarComponent,
    LargeCalendarComponent
  ],
  imports: [
    FullCalendarModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
