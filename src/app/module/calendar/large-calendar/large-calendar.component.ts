import { Component, OnInit } from '@angular/core';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-large-calendar',
  templateUrl: './large-calendar.component.html',
  styleUrls: ['./large-calendar.component.scss']
})
export class LargeCalendarComponent implements OnInit {
  options: any;
  eventsModel = [];
  constructor() { }

  ngOnInit() {
    this.eventsModel = [];
    const tempArray = [
      {contactName:'Justin',email:'justin@gmail.com',dueTime:"2020-05-12T18:30:00.000Z"},
      {contactName:'Jones',email:'jones@gmail.com',dueTime:"2020-05-14T18:30:00.000Z"},
      {contactName:'James',email:'james@gmail.com',dueTime:"2020-05-20T18:30:00.000Z"}
    ]
    this.dataArrange(tempArray);
  }
  dataArrange(val: any) {
    let newData;
    for (const data of val) {
      newData = {
        title: data.subject ? data.subject : data.task,
        description: 'description for Long Event',
        date: moment(data.dueTime).format('YYYY-MM-DD'),
        extendedProps: { ...data },
      };

      this.eventsModel.push(newData);
    }

    this.options = {
      editable: true,
      eventLimit: false,
      customButtons: {
        myCustomButton: {
          text: 'Add Tasks',
          click: 'addTask()',
        },
      },
      theme: 'standart', // default view, may be bootstrap
      header: {
        left: 'prev,title,next today myCustomButton',
        center: '',
        right: 'dayGridMonth,dayGridWeek,dayGridDay',
      },
      plugins: [dayGridPlugin, interactionPlugin],
    };
  }


  dateClick(evnt){
    console.log(evnt.dateStr)
  }

  eventClick(eventClick){
console.log(eventClick)
  }
  eventrender(){

  }

}
