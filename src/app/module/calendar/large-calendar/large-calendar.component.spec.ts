import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LargeCalendarComponent } from './large-calendar.component';

describe('LargeCalendarComponent', () => {
  let component: LargeCalendarComponent;
  let fixture: ComponentFixture<LargeCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LargeCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LargeCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
